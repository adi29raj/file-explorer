#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>

// 1. 
// Reads 1Byte of data from STDIN(console) and store it into a char 'c'
// read() and STDIN_FILENO(standad input) come from <unistd.h>
// c!='q' will ensure to stop reading if q is input from STDIN

// {
//   char c;
//   while (read(STDIN_FILENO, &c, 1) == 1 && c != 'q');
// }

// 2.
// Canonical Mode ->  keyboard input is only sent to your program when the user presses Enter
// Raw Mode -> Reads as per values are entered,needs a lot of flag to be changed to enter this mode

// 3.
// Filping flag for turning off echo
// struct termios, tcgetattr(), tcsetattr(), ECHO, and TCSAFLUSH all come from <termios.h>.

struct termios orig_termios; // kind of object of terminal

void disableRawMode() {
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
}


void enableRawMode() {
  tcgetattr(STDIN_FILENO, &orig_termios); // current terminal attributes can be read in termios using tcgetattr.


  atexit(disableRawMode); // seeting the terminal back to deffault on exit

  // ECHO is for writing the input we press on keyboard written back on terminal
  // To disable flag, first all flag are just one bit set of 4B
  // ECHO is in c_lflag field called “local flags” 
  // Now to disable a bit, ECHO is 00000000000000000000000000001000, so takes it not and && it with c_flags ccurrent value to disable the bit for ECHO in c_flags
  // ICANON flag reading input byte-by-byte, instead of line-by-line.
  orig_termios.c_lflag &= ~(ECHO | ICANON);


  // after modifying termios object, you can then apply them to the terminal using tcsetattr()
  // TCSAFLUSH  argument specifies when to apply the change,like after each input or enter also what to do with unread input
  // u can always run "reset" to revert to default attributes or reopen terminal
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios); 
}

isCntrl() // tests whether a character is a control character.

BRKINT // When BRKINT is turned on, a break condition will cause a SIGINT signal to be sent to the program, like pressing Ctrl-C.

// somehow makes read() to return while user is not inputting anything
raw.c_cc[VMIN] = 0;
raw.c_cc[VTIME] = 1;


// most library passes the description of why thwy failed and prerror prints that as the error code is kept as global variable and preeeror reads that
// prints user given string on top for debugging
void die(const char *s) {
  // \x1b is the escape seq and [ always comes with escape sequence
  // J is for clearig the scren and parameter to J is 2 means to clea the entore screem
  // <esc>[0J would clear the screen from the cursor up to the end of the screen. 
  write(STDOUT_FILENO, "\x1b[2J", 4);  // bascally scrolls dow to give a view like new screen has come
  // reposition the cursor to default argument to H(1,1)
  write(STDOUT_FILENO, "\x1b[H", 3);

  perror(s);
  exit(1);
}

abAppend(ab, "\x1b[K", 3);
// The K command (Erase In Line) erases part of the current line. 
// Its argument is analogous to the J command’s argument: 2 erases the whole line, 
// 1 erases the part of the line to the left of the cursor, 
// and 0 erases the part of the line to the right of the cursor.
//  0 is the default argument, and that’s what we want, so we 
//  leave out the argument and just use <esc>[K.


// IMPORTANT
// Refreshing the screen happens by scrolling while removing each line to right of it rewrites over it