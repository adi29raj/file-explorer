#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <bits/stdc++.h>

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <fcntl.h>
#include <cstdio>



using namespace std;

#define CTRL_KEY(k) ((k) & 0x1f)
#define ABUF_INIT {NULL, 0}

int OWNER_LENGTH = 8;
string FILE_SEPERATOR = "/";
string PARENT_PATH_EXTENSION = "/..";
string HOME_PATH = "/home";
string USER_PATH = "/home/aditya";

typedef struct erow {
  int size;
  char *chars;
} erow;

struct editorConfig {
  struct termios orig_termios;
  int screenrows;
  int screencols;
  int cx,cy;
  int numrows;
  int rowoff;
  int mode;
  erow *row;
};
struct editorConfig E;

struct traversal {
  stack<string> forwardStack;
  stack<string> backwardStack;  
  string currentPath;
};
struct traversal T;

vector<string> dirData; // keeps the onscreen all rows
vector<string> dirNamesWithPath;
string command;
string commandStatusMessage;
vector<string> commandLiterals;

struct abuf {
  char *b;
  int len;
};

enum editorKey {
  ARROW_LEFT = 'a',
  ARROW_RIGHT = 'd',
  ARROW_UP = 'w',
  ARROW_DOWN = 's',
  // END_KEY = 'o',
  COLON= ':',
  // to:do change this
  ENTER_KEY = '0', 
  // BACKSPACE = '1',
  ESC = '2',
  PAGE_UP = '3',
  PAGE_DOWN = '4',
  DEL_KEY = '5',
  BACKSPACE = '6',


  HOME_KEY = 'h',
};

enum ActionKeys {
  ENTER = 0,
  BACKWARD = 1,
  FORWARD = 2,
  PARENT =3,
  HOME = 4,
};

enum VIEW_MODE {
  NORMAL = 0,
  COMMAND = 1,
};

const string COPY = "copy";
const string MOVE = "move";
const string RENAME = "rename";
const string CREATE_DIR = "create_dir";
const string CREATE_FILE = "create_file";
const string DELETE_FILE = "delete_file";
const string DELETE_DIR = "delete_dir";
const string GOTO = "goto";
const string SEARCH ="search";

const string INVALID_COMMAND = "Enter a valid command!";
const string INVALID_OPCODE = "Enter a valid opcode!";
const string INVALID_ARGUMENTS = "Enter valid argument list!";
const string COMMAND_SUCCESS_CREATE_DIR = "Directory created successfully!";
const string COMMAND_SUCCESS_GOTO = "Command executed successfully!";
const string COMMAND_SUCCESS_CREATE_FILE = "File created successfully!";
const string COMMAND_SUCCESS_RENAME = "File renamed successfully!";
const string COMMAND_SUCCESS_SEARCH = "Search has been completed!";
const string COMMAND_SUCCESS_COPY = "Copying files has been completed!";
const string COMMAND_SUCCESS_DELETE_FILE = "File deleted successfully!";
const string COMMAND_SUCCESS_DELETE_DIR = "Directory deleted successfully!";



void abAppend(struct abuf *ab, const char*s,int len){
  char *newBf = (char *)realloc(ab->b,ab->len + len);
  if(newBf==NULL) return;
  memcpy(&newBf[ab->len],s,len); // copies after the current buffer the new string s
  ab->b = newBf;
  ab->len += len;
}

void abFree(struct abuf *ab) {
  free(ab->b);
}


void die(const char *s) {
  write(STDOUT_FILENO, "\x1b[2J", 4);
  write(STDOUT_FILENO, "\x1b[H", 3);
  perror(s);
  exit(1);
}

void disableRawMode() {
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &E.orig_termios);
}

void enableRawMode() {
  if(tcgetattr(STDIN_FILENO, &E.orig_termios) == -1 ){
    die("tcgetattr");
  }

  atexit(disableRawMode);
  struct termios raw = E.orig_termios;
  raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON); // most of these are preset for current terminals
  raw.c_oflag &= ~(OPOST); // not to add \r\n by default while otputting to terminal
  raw.c_cflag |= (CS8);
  // raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
  raw.c_lflag &= ~(ECHO | ICANON);

  raw.c_cc[VMIN] = 0;
  raw.c_cc[VTIME] = 1;
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

void editorAppendRow(string s, size_t len) {
  E.row= (erow *)realloc(E.row, sizeof(erow) * (E.numrows + 1));
  int at = E.numrows;
  E.row[at].size = len;
  E.row[at].chars = (char *)malloc(len + 1);
  memcpy(E.row[at].chars, s.c_str(), len);
  E.row[at].chars[len] = '\0';
  E.numrows++;
}

void editorScroll() {
  if (E.cy < E.rowoff) {
    E.rowoff = E.cy;
  }
  if (E.cy >= E.rowoff + E.screenrows) {
    E.rowoff = E.cy - E.screenrows + 1;
  }
}

void editorDrawRows(struct abuf *ab){
  for(int y=0;y<E.screenrows;y++){
    int filerow = y + E.rowoff;
      char lineNo[4];
      abAppend(ab, "\033[1;32m# " , 9);
      if(filerow<10){
        abAppend(ab, "00" , 2);
      }
      else if(filerow<100){
        abAppend(ab, "0" , 1);
      }
      int lineNolen = snprintf(lineNo, sizeof(lineNo),
      "%d", filerow);
      abAppend(ab, lineNo, lineNolen);
      abAppend(ab,"\033[0m",4);
      abAppend(ab, " " , 1);
      
      if(filerow<E.numrows){
        if(filerow==E.cy){
          abAppend(ab, "\033[1;34m" , 7);
        }
        int len = E.row[filerow].size;
        if (len > E.screencols) len = E.screencols;
        abAppend(ab, E.row[filerow].chars, len);
        if(filerow==E.cy){
          abAppend(ab,"\033[0m",4);
        }
      }
      abAppend(ab, "\x1b[K", 3); // removes the current line after the cursor
      abAppend(ab, "\r\n", 2); 
      // if (y < E.screenrows - 1) {
      //   abAppend(ab, "\r\n", 2); // skip "\r\n" for last line
      // }
  }
}

void editorDrawStatusBar(struct abuf *ab) {
  abAppend(ab, "\033[1;44m " , 8);
  if(E.mode == NORMAL){
    char status[80];
    string stausMsg = "NORMAL MODE : " + T.currentPath;
    int len = snprintf(status, sizeof(status), "%s",stausMsg.c_str());
    if (len > E.screencols) len = E.screencols;
    abAppend(ab, status, len);
    abAppend(ab, "    ", 4); // just to give exptra padding at end
    abAppend(ab, "\r\n", 2); 
    abAppend(ab, "\x1b[m", 3); // reset color
    abAppend(ab, "\x1b[K", 3); // removes the current line after the cursor
  }else if(E.mode == COMMAND){
    abAppend(ab, "COMMAND MODE : ", 15);
    abAppend(ab, "\x1b[K", 3); // removes the current line after the cursor
    char sMessage[256];
    int len = snprintf(sMessage, sizeof(sMessage), "%s",commandStatusMessage.c_str());
    if (len > E.screencols) len = E.screencols;
    abAppend(ab, "\033[1;31m " , 8);
    abAppend(ab, sMessage, len);
    abAppend(ab, "    ", 4); // just to give exptra padding at end
    abAppend(ab, "\x1b[m", 3); // reset color
    abAppend(ab, "\r\n", 2); 
  }
}

int displayCommand(struct abuf *ab){
  abAppend(ab, "\033[1;47m " , 8);
  abAppend(ab, "\033[1;31m " , 8);
  abAppend(ab, "\x1b[K", 3); // removes the current line after the cursor
  char cmd[512];
  int len = snprintf(cmd, sizeof(cmd), "%s",command.c_str());
  if (len > E.screencols) len = E.screencols;
  abAppend(ab, cmd, len);
  abAppend(ab, "    ", 4); // just to give exptra padding at end
  abAppend(ab, "\x1b[m", 3); // reset color
  abAppend(ab, "\r\n", 2); 
  return 0;
}


void editorRefreshScreen() {
  editorScroll();
  struct abuf ab = ABUF_INIT;
  abAppend(&ab, "\x1b[?25l", 6); // hide the cursor while refreshing
  // rather than refreshing whole screen we will remove one line at a time in editorDrawRows()
  // abAppend(&ab, "\x1b[2J", 4); // refreshes(scrolls down) the whole screen
  abAppend(&ab, "\x1b[H", 3); 
  editorDrawRows(&ab);
  editorDrawStatusBar(&ab);
  if(E.mode == COMMAND){
    displayCommand(&ab);
  }else{
    abAppend(&ab, "\x1b[H", 3); 
  }
  char buf[32];
  snprintf(buf, sizeof(buf), "\x1b[%d;%dH", (E.cy - E.rowoff) + 1, E.cx + 1);
  abAppend(&ab, buf, strlen(buf)); 

  // abAppend(&ab, "\x1b[H", 3); // taking cursor back to top
  abAppend(&ab, "\x1b[?25h", 6); // bring the cursor back
  write(STDOUT_FILENO, ab.b, ab.len);
  abFree(&ab);
}

char editorReadKey() {
  int nread;
  char c;
  while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
    if (nread == -1 && errno != EAGAIN) die("read");
  }
  // Maps arrow keys to w,a,s,d 
  if (c == '\x1b') {
    char seq[3];
    if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\x1b';
    if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\x1b';
    if (seq[0] == '[') {
      if (seq[1] >= '0' && seq[1] <= '9') {
        if (read(STDIN_FILENO, &seq[2], 1) != 1) return '\x1b';
        if (seq[2] == '~') {
          switch (seq[1]) {
            case '1': return HOME_KEY;
            case '3': return DEL_KEY;
            // case '4': return END_KEY;
            case '5': return PAGE_UP;
            case '6': return PAGE_DOWN;
            case '7': return HOME_KEY;
            // case '8': return END_KEY;
          }
        }
      }
      else{
        switch (seq[1]) {
          case 'A': return ARROW_UP;
          case 'B': return ARROW_DOWN;
          case 'C': return ARROW_RIGHT;
          case 'D': return ARROW_LEFT;
          case 'H': return HOME_KEY;
        }
      }
      
    }
    return '\x1b';
  }else {
    if(c == '\r'){
      return ENTER_KEY;
    }
    if(c == 127){
      return BACKSPACE;
    }
    if(c== ';'){  // to:do change it to use escape
      return ESC;
    }
    if(c == ':'){
      return COLON;
    }
    return c;
  }
}

int getWindowSize(int *rows, int *cols) {
  struct winsize ws;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
    return -1;
  } else {
    *cols = ws.ws_col;
    *rows = ws.ws_row;
    return 0;
  }
}

void writeToEditor(vector<string>& data) {
  std::vector<std::string> t;
  int n = data.size();
  for(string s : data){
      editorAppendRow(s,s.size());
  }
}

void editorMoveCursor(int key) {
  switch (key) {
    case ARROW_LEFT:
      if (E.cx != 0) {
        E.cx--;
      }
      break;
    case ARROW_RIGHT:
      if (E.cx != E.screencols - 1) {
        E.cx++;
      }
      break;
    case ARROW_UP:
      if (E.cy != 0) {
        E.cy--;
      }
      break;
    case ARROW_DOWN:
      if (E.cy < E.numrows - 1) {
        E.cy++;
      }
      break;
  }
}

string getAbsolutePath(string relPath){
  char absPath[PATH_MAX+1];
  const char *path = relPath.c_str();
  char *p = realpath(path, absPath);
  if(p==NULL){
    string msg = "Error getting absolute path(getAbsolutePath) " + relPath;
    commandStatusMessage = msg;
    return to_string(1);
    // die(msg.c_str());
  }
  string abs = string(p);
  // cout << relPath << " " << absPath << endl;
  return abs;
}

int openDirectory(vector<string>& dirAbsPath,string dirPath){
  struct dirent *pDirent;
  DIR *pDir;
  // string absPath = getAbsolutePath(dirPath);
  string absPath =dirPath;
  //  if(!isAbsolutePath){
  //   char *p = realpath(path, absPath);
  //   if(p==NULL){
  //     die("openDirectory");
  //   }
  // }else{
  //   // not sure if working
  //   int i=0;
  //   while(path[i]!='\0'){
  //     absPath[i] = path[i];
  //     i++;
  //   }
  // }

  pDir = opendir(absPath.c_str());
  if (pDir == NULL) {
    printf("Cannot open directory %s.", absPath.c_str());
    return 1;
  }
  string dName = "";
  while((pDirent = readdir(pDir)) != NULL) {
    dName=pDirent->d_name;
    dirAbsPath.push_back(absPath + FILE_SEPERATOR + dName);
  }
  closedir (pDir);
  return 0;
}

string formatFileSize(int fileSize){
  string fs = "";
  if(fileSize==0){
    return "000KB";
  }
  if(fileSize > 1024){
    fileSize/=1024;
    fs = to_string(fileSize) + "KB";
  }if(fileSize > 1024){
    fileSize/=1024;
    fs = to_string(fileSize) + "MB";
  }
  if(fs.size()==3){
    fs = "00" + fs;
  }
  if(fs.size()==4){
    fs = "0" + fs;
  }
  if(fs.size() == 0){
     return "-----";
  }
  return fs;
}

int getFileDetail(string filePath, vector<string>& dirData){
  struct stat fileStat;
  const char *path = filePath.c_str();
  if(stat(path, &fileStat) < 0){ 
    return 1;
  }
  string rowData = "";
  string permission = "";
  S_ISDIR(fileStat.st_mode) ? permission.push_back('d') : permission.push_back('-');
  (fileStat.st_mode & S_IRUSR) ? permission.push_back('r') : permission.push_back('-');
  (fileStat.st_mode & S_IWUSR) ? permission.push_back('w') : permission.push_back('-');
  (fileStat.st_mode & S_IXUSR) ? permission.push_back('x') : permission.push_back('-');
  (fileStat.st_mode & S_IRGRP) ? permission.push_back('r') : permission.push_back('-');
  (fileStat.st_mode & S_IWGRP) ? permission.push_back('w') : permission.push_back('-');
  (fileStat.st_mode & S_IXGRP) ? permission.push_back('x') : permission.push_back('-');
  (fileStat.st_mode & S_IROTH) ? permission.push_back('r') : permission.push_back('-');
  (fileStat.st_mode & S_IWOTH) ? permission.push_back('w') : permission.push_back('-');
  (fileStat.st_mode & S_IXOTH) ? permission.push_back('x') : permission.push_back('-');

  string noOfLink = to_string(fileStat.st_nlink);
  if(noOfLink.size()==1){
    noOfLink = "0" + noOfLink;
  }
  string fileSize = formatFileSize(fileStat.st_size);
  string fileName = filePath;

  char mTime[20];
  struct tm * timeinfo;
  timeinfo = localtime(&(fileStat.st_mtime)); 
  strftime(mTime, 20, "%b %d %H:%M", timeinfo);

  struct group *grp;
  struct passwd *pwd;
  grp = getgrgid(fileStat.st_gid);
  pwd = getpwuid(fileStat.st_uid);

  string grpName = grp->gr_name;
  string usrName = pwd->pw_name;
  while(grpName.size() <= OWNER_LENGTH && grpName.size()!=OWNER_LENGTH){
    grpName+=" ";
  }
  while(usrName.size() <= OWNER_LENGTH && usrName.size()!=OWNER_LENGTH){
    usrName+=" ";
  }

  rowData = permission + "  " + noOfLink + "  " + grpName + "  " + usrName + "  " + fileSize + "  " + mTime +  " \t" + fileName;

  dirData.push_back(rowData);

  return 0;
}


int handleNormalMode(vector<string>& dirData){
  openDirectory(dirNamesWithPath,T.currentPath);
  sort(dirNamesWithPath.begin(),dirNamesWithPath.end());
  for(int i=0;i<dirNamesWithPath.size();i++){
    getFileDetail(dirNamesWithPath[i],dirData);
  }
  return 0;
}

int handleTraversal(int actionKey,string newDirPath = ""){
  if(actionKey == ENTER){
    T.backwardStack.push(T.currentPath);
    string absPath = getAbsolutePath(newDirPath);
    if(absPath == to_string(1)){
      return 1;
    }
    T.currentPath = absPath;
  }
  if(actionKey == BACKWARD) {
    if(T.backwardStack.empty() || T.currentPath ==  T.backwardStack.top()){
      return 1;
    }
    T.forwardStack.push(T.currentPath);
    T.currentPath = T.backwardStack.top();
    T.backwardStack.pop();
  }
  if(actionKey == FORWARD){
    if(T.forwardStack.empty() || T.currentPath ==  T.forwardStack.top()){
      return 1;
    }
    T.backwardStack.push(T.currentPath);
    T.currentPath = T.forwardStack.top();
    T.forwardStack.pop();
  }
  if(actionKey == PARENT){
    T.backwardStack.push(T.currentPath);
    string absPath = getAbsolutePath(T.currentPath + PARENT_PATH_EXTENSION);
    if(absPath == to_string(1)){
      return 1;
    }
    T.currentPath = absPath;
  }
  if(actionKey == HOME){
    T.backwardStack.push(T.currentPath);
    T.currentPath = HOME_PATH;
  }
  return 0;
}

string getSelectedFile(int y){
  return dirNamesWithPath[y];
}

bool isDirectory(string file){
  struct stat fileStat;
  const char *path = file.c_str();
  if(stat(path, &fileStat) < 0){ 
    return 1;
  }
  if(S_ISDIR(fileStat.st_mode))
    return true;
  return false;
}

int openFileWithDefaultPgm(string sFile){
  const char *the_file = sFile.c_str();
  int pid = fork();
  if (pid == 0) {
    execl("/usr/bin/xdg-open", "xdg-open", the_file, (char *)NULL);
    exit(1);
  }
  return 0;
}

void initEditor() {
  E.cx = 0;
  E.cy = 0;
  E.numrows = 0;
  E.row = NULL;
  E.rowoff = 0;
  command="";
  commandStatusMessage="";
  if (getWindowSize(&E.screenrows, &E.screencols) == -1) die("getWindowSize");
  E.screenrows -= 3;
  write(STDOUT_FILENO, "\x1b[2J", 4);
}

void flushCurrentData(){
  dirData.clear();
  dirNamesWithPath.clear();
  editorRefreshScreen();
}

int openNewDirectory(){
  flushCurrentData();
  initEditor();
  handleNormalMode(dirData);
  writeToEditor(dirData);
  return 0;
}

void handleEnterClick(){
  int y = E.cy;
  if(y <= E.numrows){
    string sFile = getSelectedFile(E.cy);
    if(isDirectory(sFile)){
      handleTraversal(ENTER, sFile);
      openNewDirectory();
    }else{
      openFileWithDefaultPgm(sFile);
    }
  }
}

int handleBackspaceKey(){
  handleTraversal(PARENT);
  openNewDirectory();
  return 0;
}

int handleHomeKey(){
  handleTraversal(HOME);
  openNewDirectory();
  return 0;
}

///////////////////////////////////////////////////////////////////////////////

int enterCommandMode(){
  E.mode = 1;
  return 0;
}

int breakCommandMode(){
  E.mode = 0;
  return 0;
}

int extractLiterals(string cmd){
  int i=0;
  string temp = "";
  while(cmd[i]!='\0'){
    if(cmd[i]==' '){
      commandLiterals.push_back(temp);
      temp = "";
    }else{
      temp+=cmd[i];
    }
    i++;
  }
  if(temp!=""){
    commandLiterals.push_back(temp);
  }
  return 0;
}

string getFinalPath(string curPath, string relPath){
  if(relPath[0] == '~'){ // handling tilde ~
    relPath.erase(0,1);
    relPath = USER_PATH + relPath;
  }
  string absPath ;
  std::filesystem::path path(relPath);
  if (path.is_relative()) {
    absPath = getAbsolutePath(curPath + FILE_SEPERATOR + relPath);
  }
  if (path.is_absolute()) {
    absPath = getAbsolutePath(relPath);
  }
  return absPath;
}

int handleGOTO(vector<string>& commandLiterals){
  if(commandLiterals.size()!=2){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  string absPath = getFinalPath(T.currentPath,commandLiterals[1]);
  if(absPath == to_string(1)){
      return 1;
  }
  T.currentPath = absPath;
  openNewDirectory();
  return 0;
}

int handleCREATE_DIR(vector<string>& commandLiterals){
  if(commandLiterals.size()!=3){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  string dirName = commandLiterals[1];
  string destPath =  getFinalPath(T.currentPath,commandLiterals[2]);
  if(destPath == to_string(1)){
    return 1;
  }
  string fDir = destPath + FILE_SEPERATOR + dirName;
  int status= mkdir(fDir.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(status==-1){
    commandStatusMessage = "Error in creating the Directory in  " + fDir;
    return -1;
  }
  // goto the path where new directory is created.
  T.currentPath = destPath;
  openNewDirectory();
  return 0;
}

int handleCREATE_FILE(vector<string>& commandLiterals){
  if(commandLiterals.size()!=3){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  string fileName = commandLiterals[1];
  string destPath =  getFinalPath(T.currentPath,commandLiterals[2]);
  if(destPath == to_string(1)){
    return 1;
  }
  string fFile = destPath + FILE_SEPERATOR + fileName;
  int status= open(fFile.c_str(),O_RDONLY | O_CREAT,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ); 	
  if (status == -1){
      commandStatusMessage = "Error in creating new file" + fFile;	
      return -1;       
  }
  // goto the path where new directory is created.
  T.currentPath = destPath;
  openNewDirectory();
  return 0;
}

int handleRENAME(vector<string>& commandLiterals){
  if(commandLiterals.size()!=3){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  string curFile =  getFinalPath(T.currentPath,commandLiterals[1]);
  if(curFile == to_string(1)){
    return 1;
  }
  string curFilePath = curFile;
  int i=curFilePath.length()-1;
  string temp="";
  while(curFilePath[i]!='/'){
    temp+=curFilePath[i];
    curFilePath.pop_back();
    i--;
  }
  curFilePath.pop_back();
  string newFilePath = curFilePath + FILE_SEPERATOR + commandLiterals[2];
  rename(curFile.c_str(),newFilePath.c_str());
  T.currentPath = curFilePath;
  openNewDirectory();
  return 0;
}

void reverseStr(string& str){
    int n = str.length();
    for (int i = 0; i < n / 2; i++)
        swap(str[i], str[n - i - 1]);
}

string extractFileNameFromAbsPath(string absPath){
  int i=absPath.length()-1;
  string temp="";
  while(absPath[i]!='/'){
    temp+=absPath[i];
    i--;
  }
  reverseStr(temp);
  return temp;
}

void search(string absPath,string searchKey,vector<pair<string,int>>& searchedPath){
  vector<string> currentDirList;
  openDirectory(currentDirList,absPath);
  sort(currentDirList.begin(),currentDirList.end());
  string fName;
  struct stat fileStat;
  int index=-1;
  for(string it : currentDirList){
    index++;
    fName = extractFileNameFromAbsPath(it);
    if(fName == searchKey){
      searchedPath.push_back(make_pair(it,index));
      return;
    }
    else if(fName == "." || fName == ".."){
      continue;
    }
    else {
      const char *path = it.c_str();
      if(stat(path, &fileStat) < 0){ 
        continue;
      }
      if(S_ISDIR(fileStat.st_mode)){
        search(it,searchKey,searchedPath);
      }
    }
  }
}

string removeFileNameFromPath(string path){
  int i=path.length()-1;
  while(path[i]!='/'){
    path.pop_back();
    i--;
  }
  path.pop_back();
  return path;
}

int handleSEARCH(vector<string>& commandLiterals){
  if(commandLiterals.size()!=2){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  vector<pair<string,int>> searchedPath;
  search(T.currentPath,commandLiterals[1],searchedPath);
  if(searchedPath.size()>0){
    T.currentPath = removeFileNameFromPath(searchedPath[0].first); // showing the first occurence
    openNewDirectory();
    E.cy =searchedPath[0].second;
    commandStatusMessage = to_string(searchedPath.size()) + " occurences found!";
  }else{
    commandStatusMessage = "No occurences found!";
  }
  return 0;
}

int copyFile(string srcPath,string destPath){
  char block[1024];
  int in,out;
  int st;
  string fileName = extractFileNameFromAbsPath(srcPath);
  string destPathWithFileName = destPath + FILE_SEPERATOR + fileName;
  in = open(srcPath.c_str(), O_RDONLY);
	out = open(destPathWithFileName.c_str(), O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR);
  while((st = read(in,block,sizeof(block))) > 0){
		write(out,block,st);
  }
  struct stat sourceStat,destStat;
	if (stat(srcPath.c_str(),&sourceStat) != -1) {  
    commandStatusMessage = "Not able to get stat details about source " + srcPath;
    return 1;
  }
  if (stat(destPathWithFileName.c_str(),&destStat) != -1) {  
    commandStatusMessage = "Not able to get stat details about destiaton " + destPathWithFileName;
    return 1;
  }
    
  int statusSrc=chown(destPathWithFileName.c_str(),sourceStat.st_uid, sourceStat.st_gid);
	if(statusSrc!=0){
    commandStatusMessage = "Not able to set owenership(chown)!";
    return 1;
  }
  int status2=chmod(destPathWithFileName.c_str(),sourceStat.st_mode);
	if(status2!=0){
		commandStatusMessage = "Not able to set permission(chod)!";
    return 1;
  }
  return 0;
}

int copyDirectory(string srcPath,string destPath){
  string folderName = extractFileNameFromAbsPath(srcPath);
  string destPathWithFolderName = destPath + FILE_SEPERATOR + folderName;
  int status= mkdir(destPathWithFolderName.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(status==-1){
    commandStatusMessage = "Error in creating the Directory in  " + destPath;
    return -1;
  }
  struct dirent *pDirent;
  DIR *pDir;
  pDir = opendir(srcPath.c_str());
  if (pDir == NULL) {
    commandStatusMessage = "Not table to open src directory  " + srcPath;
    return 1;
  }
  string dName="";
  while((pDirent = readdir(pDir)) != NULL) {
    dName=pDirent->d_name;
    if(dName == "." || dName == ".."){
      continue;
    }
    else{
      string srcPathWithName = srcPath + FILE_SEPERATOR + dName;
      struct stat sStat;
      if(stat(srcPathWithName.c_str(),&sStat)!=-1){
        if(S_ISDIR(sStat.st_mode)){
          copyDirectory(srcPathWithName,destPathWithFolderName);
        }else{
          copyFile(srcPathWithName,destPathWithFolderName);
        }
      }else{
        commandStatusMessage = "Not table to get details of  " + srcPathWithName + " in source directory";
        return 1;
      }
    }
  }
  return 0;
}

int copy(string srcPath,string destPath){
    const char *path = srcPath.c_str();
    struct stat fileStat;
    if(stat(path, &fileStat) < 0){ 
      return 1;
    }
    // if directory
    if(S_ISDIR(fileStat.st_mode)){
      copyDirectory(srcPath,destPath);
    }
    // if file
    else{
      copyFile(srcPath,destPath);
    }
  return 0;
}

int handleCOPY(vector<string>& commandLiterals){
  int length = commandLiterals.size();
  string desFile = commandLiterals[length - 1];
  string desPath =  getFinalPath(T.currentPath,desFile);
  if(desPath == to_string(1)){
    return 1;
  }
  for(int i=1; i<length - 1;i++){
    string srcPath = getFinalPath(T.currentPath,commandLiterals[i]);
    if(srcPath == to_string(1)){
      return 1;
    }
    copy(srcPath,desPath);
  }
  T.currentPath = desPath;
  openNewDirectory();
  return 0;
}

int deleteFile(string path){
  int status = remove(path.c_str());
  if(status != 0){
	 	commandStatusMessage = "Not able to delete file " + path;
    return 1;
	}
  return 0;
}

int handleDELETE_FILE(vector<string>& commandLiterals){
  if(commandLiterals.size()!=2){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  string delpath = getFinalPath(T.currentPath,commandLiterals[1]);
  if(delpath == to_string(1)){
      return 1;
  }
  if(deleteFile(delpath)==0){
    T.currentPath = removeFileNameFromPath(delpath);
    openNewDirectory();
  }else{
    return 1;
  }
  return 0;
}

int deleteDir(string dirPath){
  struct dirent *pDirent;
  DIR *pDir;
  pDir = opendir(dirPath.c_str());
  if (pDir == NULL) {
    commandStatusMessage = "Not table to open directory  " + dirPath;
    return 1;
  }
  string dName="";
  while((pDirent = readdir(pDir)) != NULL) {
    dName=pDirent->d_name;
    if(dName == "." || dName == ".."){
      continue;
    }
    else{
      string pathWithName = dirPath + FILE_SEPERATOR + dName;
      struct stat sStat;
      if(stat(pathWithName.c_str(),&sStat)!=-1){
        if(S_ISDIR(sStat.st_mode)){
          deleteDir(pathWithName);
        }else{
          deleteFile(pathWithName);
        }
      }else{
        commandStatusMessage = "Not table to get details of  " + pathWithName + " in source directory";
      return 1;
      }
    }
  }
  return 0;
}

int handleDELETE_DIR(vector<string>& commandLiterals){
  if(commandLiterals.size()!=2){
    commandStatusMessage = INVALID_ARGUMENTS;
    return 1;
  }
  string dirPath = getFinalPath(T.currentPath, commandLiterals[0]);
  if(dirPath == to_string(1)){
      return 1;
  }
  if(deleteDir(dirPath) != 0){
    return 1;
  }
  return 0;
}

int executeCommand(string& command){
  command.push_back('\0');
  extractLiterals(command);
  string opcode = commandLiterals[0];
  if(opcode == GOTO){
    if(handleGOTO(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_GOTO;
    }
  }
  else if(opcode == CREATE_DIR){
    if(handleCREATE_DIR(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_CREATE_DIR;
    }
  }
  else if(opcode == CREATE_FILE){
    if(handleCREATE_FILE(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_CREATE_FILE;
    }
  }
  else if(opcode == RENAME){
    if(handleRENAME(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_RENAME;
    }
  }
  else if(opcode == SEARCH){
    if(handleSEARCH(commandLiterals)==0){
      // commandStatusMessage=COMMAND_SUCCESS_SEARCH;
    }
  }
  else if(opcode == COPY){
    if(handleCOPY(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_COPY;
    }
  }
  else if(opcode == DELETE_FILE){
    if(handleDELETE_FILE(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_DELETE_FILE;
    }
  }
  else if(opcode == DELETE_DIR){
    if(handleDELETE_DIR(commandLiterals)==0){
      commandStatusMessage=COMMAND_SUCCESS_DELETE_DIR;
    }
  }
  else{
    commandStatusMessage = INVALID_OPCODE;
  }
  command = "";
  commandLiterals.clear();
  return 0;
}
int setCommand(string& command,char c){
  commandStatusMessage = "";
  if(c==BACKSPACE){
    if(command.size()){
      command.pop_back();
    }
  }else{
    command.push_back(c);
  }
  return 0;
}

int handleCommandMode(int c){
   if(c == CTRL_KEY('q')){
      write(STDOUT_FILENO, "\x1b[2J", 4);
      exit(0);
    }
    if(c == ESC ){
      breakCommandMode();
    }
    if(c == ENTER_KEY){
      executeCommand(command);
    }
    else {
      setCommand(command,c);
    }

  return 0;
}

//////////////////////////////////////////////////////////////////////////////

void editorProcessKeypress() {
  char c = editorReadKey();
  if(E.mode == COMMAND){
    handleCommandMode(c);
  }
  else {
    switch (c) {
      case CTRL_KEY('q'):
        write(STDOUT_FILENO, "\x1b[2J", 4);
        exit(0);
        break;
      case ARROW_LEFT:
        editorMoveCursor(c);
        if(handleTraversal(BACKWARD) == 0 ){
          openNewDirectory();
        }
        break;
      case ARROW_RIGHT:
        editorMoveCursor(c);
        if(handleTraversal(FORWARD) == 0){
          openNewDirectory();
        }
        break;
      case ARROW_UP:
      case ARROW_DOWN:
        editorMoveCursor(c);
        break;
      case ENTER_KEY: 
        handleEnterClick();
        break;
      case BACKSPACE:
        handleBackspaceKey();
        break;
      case HOME_KEY:
        handleHomeKey();
        break;
      case COLON:
        enterCommandMode();
        break;
      case ESC:
        breakCommandMode();
        break;
    }
  }
  
}

void initTraversal(){
  T.currentPath=getAbsolutePath(".");
}


int main(){
  E.mode=0;
  initEditor();
  initTraversal();
  enableRawMode();
  handleNormalMode(dirData);
  writeToEditor(dirData);
  while (1) {
    editorRefreshScreen();
    editorProcessKeypress();
  } 
}